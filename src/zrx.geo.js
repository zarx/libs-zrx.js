/*
 * 
 *      zrx.geolocation.js
 *      Adds geolaction support
 *      Requirements: jQuery, zrx.common.js
 * 
 */

var zrx = zrx || {};
zrx.geolocation = zrx.geolocation || {};

zrx.geolocation.errorCallbacks = [];
zrx.geolocation.successCallbacks = [];
zrx.geolocation.currentLocation = null;

zrx.geolocation.GetDistance = function (lat1, lon1, lat2, lon2, unit) {
    var R;
    unit = unit || 'km';
    
    switch (unit)
    {
        case 'm':
        case 'mi':
            R = 3959; // miles
            break;
        case 'k':
        case 'km':
        default:
            R = 6371; // km
            break;
    }
    
    lat1 = parseFloat(lat1);
    lat2 = parseFloat(lat2);
    lon1 = parseFloat(lon1);
    lon2 = parseFloat(lon2);
    
    var dLat = (lat2 - lat1).toRad();
    var dLon = (lon2 - lon1).toRad();
    lat1 = lat1.toRad();
    lat2 = lat2.toRad();

    var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
			Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1) * Math.cos(lat2);
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    var d = R * c;

    return d;
};

zrx.geolocation.OnSuccess = function (pos) {
	var loc = {};
	var crd = pos.coords;
	loc.lat = crd.latitude;
	loc.lng = crd.longitude;
    zrx.geolocation.successCallback(loc);
};

zrx.geolocation.successCallback = function(loc) {
    zrx.geolocation.currentLocation = loc;
    var callbacks = zrx.geolocation.successCallbacks;
	if (zrx.geolocation.successCallbacks !== null) {
        console.log(callbacks.length);
        while (callbacks.length) {
            var successCallback = callbacks.pop();
            successCallback(loc);
        }
	}
};

zrx.geolocation.OnError = function (error) {
    var loc = {};
    if (typeof (google) !== "undefined" && google.loader.ClientLocation && zrx.geolocation.successCallback !== null) {
        console.info('Fallback google');
        loc.lat = google.loader.ClientLocation.latitude;
        loc.lng = google.loader.ClientLocation.longitude;
        zrx.geolocation.successCallback(loc);
    } else {
        console.info('Fallback ipinfo.io');
        $.get("http://ipinfo.io", function(response) {
            if (response.loc !== "undefined") {
                response.loc = response.loc.split(',');
                loc.lat = response.loc[0];
                loc.lng = response.loc[1];
                zrx.geolocation.successCallback(loc);
            } else if (typeof (zrx.geolocation.errorCallbacks) !== "undefined") {
                while (zrx.geolocation.errorCallbacks.length) {
                    var errorCallback = zrx.geolocation.errorCal	lbacks.pop();
                    errorCallback(error);
                }
            }
        }, "jsonp"); 
    }
};

zrx.geolocation.GetLocation = function (successCallback) { zrx.geolocation.GetLocation(successCallback, function (err) { }); };
zrx.geolocation.GetLocation = function (successCallback, errorCallback) { zrx.geolocation.GetLocation(successCallback, errorCallback, { }); };
zrx.geolocation.GetLocation = function (successCallback, errorCallback, options) {
	errorCallback = typeof (errorCallback) !== 'undefined' ? errorCallback : function (err) { };
	options = typeof (options) !== 'undefined' ? options : {};
    
	zrx.geolocation.successCallbacks.push(successCallback);
	zrx.geolocation.errorCallbacks.push(errorCallback);
	if (typeof (navigator) !== "undefined") {
		var ret = navigator.geolocation.getCurrentPosition(zrx.geolocation.OnSuccess, zrx.geolocation.OnError, options);
        /*
        setTimeout(function() {
            if (zrx.geolocation.currentLocation === null) {
                zrx.geolocation.OnError({ message: "User denied Geolocation", code: 1, PERMISSION_DENIED: 1, POSITION_UNAVAILABLE: 2, TIMEOUT: 3 });
            }
        }, 3000);
        */
    } else {
		zrx.geolocation.OnError({ message: "User denied Geolocation", code: 1, PERMISSION_DENIED: 1, POSITION_UNAVAILABLE: 2, TIMEOUT: 3 });
	}
};