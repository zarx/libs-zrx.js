/**
 * @file zrx.common.js, Common and core functions
 * @author <a href="mailto:hi@johan.email">Johan Boström</a>, <a href="http://johan.ninja/"> http://johan.ninja/ </a>
 * @version 1.0
 */

/**
 * ZarX namespace
 * @namespace
 */
var zrx = zrx || {};

zrx.vars = zrx.vars || {};

/** 
 * The root document
 * @type {Object} 
 */
zrx.vars.doc = zrx.vars.doc || document.documentElement;

/**
 * Event handler
 */
zrx.events = zrx.events || {};
zrx.events.add = zrx.events.add || function(elem, type, eventHandle) {
    if (elem == null || typeof(elem) == 'undefined') return;
    if ( elem.addEventListener ) {
        elem.addEventListener( type, eventHandle, false );
    } else if ( elem.attachEvent ) {
        elem.attachEvent( "on" + type, eventHandle );
    } else {
        elem["on"+type]=eventHandle;
    }
};

zrx.browser = zrx.browser || {};

(function (a) {
    zrx.browser.isMobile = /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))
    zrx.browser.isMozilla = /mozilla/.test(a.toLowerCase()) && !/webkit/.test(a.toLowerCase());
	zrx.browser.isWebkit = /webkit/.test(a.toLowerCase());
	zrx.browser.isOpera = /opera/.test(a.toLowerCase());
	zrx.browser.isMSIE = /msie/.test(a.toLowerCase());
	zrx.browser.isTrident = /trident/.test(a.toLowerCase());
})(navigator.userAgent || navigator.vendor || window.opera);

/**
 * Get parameter from url
 * @param {String} Name
 * @returns {String} Value
 */
zrx.browser.getParameterByName = zrx.browser.getParameterByName ||  function (name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
};

/**
 * Handle window size
 */

zrx.window = zrx.window || {};
zrx.window.types = zrx.window.types || {
    xs: 0,
    sm: 768,
    md: 992,
    lg: 1200,
};

zrx.window.getTypeName = zrx.window.getTypeName || function (windowType) {
    if (typeof(windowType) === "undefined") {
        windowType = zrx.window.type;
    }
    
    for (var typeName in zrx.window.types) {
        if (windowType == zrx.window.types[typeName]) {
            return typeName;
        }
    };
};

zrx.window.setType = zrx.window.setType || function () {
    var prevType = 0;
    
    for (var typeName in zrx.window.types) {
        if (zrx.window.innerWidth >= prevType && zrx.window.innerWidth < zrx.window.types[typeName]) {
            break;
        }
        prevType = zrx.window.types[typeName];
    };
    zrx.window.type = prevType;
};

zrx.window.setSizes = zrx.window.setSizes || function () {
    zrx.window.innerWidth = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    zrx.window.innerHeight = window.innerHeight || document.documentElement.innerHeight || document.body.innerHeight;
    zrx.window.innerSize = {
        width: zrx.window.innerWidth,
        height: zrx.window.innerHeight
    };
    
    zrx.window.setType();
};
zrx.events.add(window, "resize", zrx.window.setSizes);
zrx.window.setSizes();

/**
 * Randomizes a number between val1 and val2
 * @param {Number} val1
 * @param {Number} val2
 * @returns {Number}
 * @example // Returns a number between 1 and 10
 * @example zrx.rand(1, 10)
 */
zrx.rand = zrx.rand || function(val1, val2) {
    if (val2 === "undefined") {
        val2 = val1;
        val1 = 0;
    }
    return Math.floor((Math.random()*parseInt(val2))+parseInt(val1));
};

/**
 * Merges two or more objects together
 * @param {Object} var1
 * @param {Object} var2
 * @param {...Object} vars
 * @return {Object} Merged object
 */
zrx.merge = zrx.merge || function(){
    var output = {};
    
    for (var i = 0; i < arguments.length; i++) {
        for (var attrname in arguments[i]) { output[attrname] = arguments[i][attrname]; }
    }
    
    return output; 
};

/**
 * Returns type of obj
 * @param {Object} obj
 * @returns {string} Type of object as string
 */
zrx.getType = zrx.getType || function (obj) {
    return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase();
};



zrx.body = zrx.body || {};

/**
 * Checks if body has a vertical scroll
 * @returns {Boolean}
 */
zrx.body.hasVerticalScroll = zrx.body.hasVerticalScroll || function () {
    var cStyle = document.body.currentStyle || window.getComputedStyle(document.body, "");

    var hasVScroll  = cStyle.overflow == "visible" 
                    || cStyle.overflowY == "visible"
                    || (hasVScroll && cStyle.overflow == "auto")
                    || (hasVScroll && cStyle.overflowY == "auto");
            
    return hasVScroll;
};

/**
 * Returns position of scrolling
 * @returns {Object} .left and .top
 */
zrx.body.scroll = zrx.body.scroll || function () {
    var left = (window.pageXOffset || zrx.vars.doc.scrollLeft) - (zrx.vars.doc.clientLeft || 0);
    var top = (window.pageYOffset || zrx.vars.doc.scrollTop)  - (zrx.vars.doc.clientTop || 0);
    
    return {
        left: left,
        top: top
    };
};


if(typeof String.prototype.trim !== 'function') { // Adds trim function to IE 7 and 8
    /**
     * Trims a string
     * @returns {String}
     */
    String.prototype.trim = function() {
      return this.replace(/^\s+|\s+$/g, ''); 
    };
}

if(typeof String.prototype.toInt !== 'function') {
    /**
     * Converts a string to integer
     * @returns {Number}
     */
    String.prototype.toInt = function() {
      return parseInt(this); 
    };
}

if(typeof String.prototype.toFloat !== 'function') {
    /**
     * Converts a string to float
     * @returns {Number}
     */
    String.prototype.toFloat = function() {
      return parseFloat(this); 
    };
}

if (typeof (Number.prototype.toRad) === "undefined") {
    /**
     * Converts a number to radius
     * @returns {Number}
     */
    Number.prototype.toRad = function () {
        return this * Math.PI / 180;
    };
}

if (typeof (Number.prototype.toDeg) === "undefined") {
    /**
     * Converts a number to degrees
     * @returns {Number}
     */
    Number.prototype.toDeg = function () {
        return this * 180 / Math.PI;
    };
}

/**
 * Adds console function when it is missing
 * ex. IE v <= 7
 */
if (typeof console === "undefined") {
    window.console = {
        log: function () { }
    };
}

/**
 * Adds indexOf function when it is missing
 * ex. IE v <= 8
 */
if(!Array.prototype.indexOf) {
    Array.prototype.indexOf = function(needle) {
        for(var i = 0; i < this.length; i++) {
            if(this[i] === needle) {
                return i;
            }
        }
        return -1;
    };
}